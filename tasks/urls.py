from rest_framework.routers import DefaultRouter
from tasks import apis

router = DefaultRouter()

router.register(r"users", apis.CustomUserViewSet, basename="user")
router.register(r"tasks", apis.TaskViewSet, basename="task")
router.register(r"goals", apis.GoalViewSet, basename="goal")

urlpatterns = router.urls
