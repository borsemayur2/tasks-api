from django.contrib.auth.models import AbstractUser
from django.db import models


class CustomUser(AbstractUser):

    INACTIVE = 0
    ACTIVE = 1

    CHOICES = (
        (INACTIVE, "Inactive"),
        (ACTIVE, "Active"),
    )

    name = models.CharField(max_length=255)
    email = models.EmailField()
    status = models.IntegerField(choices=CHOICES, default=ACTIVE)


class Goal(models.Model):

    name = models.CharField(max_length=255)
    description = models.CharField(max_length=255)
    target_date = models.DateField()


class Task(models.Model):

    name = models.CharField(max_length=255)
    dependant_task = models.ManyToManyField(
        "self", related_name="related_tasks", blank=True
    )
    start_date = models.DateField()
    end_date = models.DateField()
    assignee = models.ForeignKey(
        CustomUser, related_name="tasks", on_delete=models.CASCADE
    )
    goal = models.ForeignKey(Goal, related_name="tasks", on_delete=models.DO_NOTHING)

    NEW = 0
    WIP = 1
    REVIEW = 2
    DONE = 3
    REOPEN = 4

    STATUS_CHOICES = (
        (NEW, "New"),
        (WIP, "Work in progress"),
        (REVIEW, "Review"),
        (DONE, "Done"),
        (REOPEN, "Reopen"),
    )
    status = models.IntegerField(choices=STATUS_CHOICES)

    LOW = 0
    MEDIUM = 1
    HIGH = 2
    CRITICAL = 3

    PRIORITY_CHOICES = (
        (LOW, "Low"),
        (MEDIUM, "Medium"),
        (HIGH, "High"),
        (CRITICAL, "Critical"),
    )
    priority = models.IntegerField(choices=PRIORITY_CHOICES)


class Project(models.Model):
    name = models.CharField(max_length=255)


class Comment(models.Model):
    title = models.CharField(max_length=100)
    content = models.CharField(max_length=255)
    task = models.ForeignKey(Task, related_name="comments", on_delete=models.CASCADE)
