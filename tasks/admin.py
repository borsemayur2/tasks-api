from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import CustomUser, Goal, Task, Comment


# Register your models here.
class CustomUserAdmin(UserAdmin):
    model = CustomUser


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Goal)
admin.site.register(Task)
admin.site.register(Comment)
