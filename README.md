# Tasks Api

## Prerequisites

1. `python3.8.5`


## Steps

1. `python3 -m venv [VENV FOLDER NAME]`
2. `source venv/bin/activate`
3. `pip install -r requirements.txt`
4. `python manage.py migrate`
5. `python manage.py runserver`

## APIs

### `/api/users`
### `/api/tasks`
### `/api/goals`
